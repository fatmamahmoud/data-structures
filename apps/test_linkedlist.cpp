#include <iostream>
#include " member1.hpp"
#include " member2.hpp"
#include " member3.hpp"
#include " member4.hpp"
#include " member5.hpp"

int main ()
{
    LL::IntegerLL App;
    if ( isEmpty (App) )
    {
        std::cout<<"linkedlist is Empty" <<std::endl;
        LL::insertFront (App , 1 );
        LL::printAll ( App );
    }
    else
    {
       LL::size ( App );
       LL::insertFront (App , 1 );
       LL::insertFront (App , 11 );
       LL::insertFront (App , 111 );
       LL::printAll ( App );
    }
    LL::getAt ( App , 2 );
    LL::removeAt ( App , 2 );
    LL::printAll ( App );
}