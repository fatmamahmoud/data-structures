#include "mathematics.hpp" // for mathematics::heron
#include <iostream> // for std::cout
#include <algorithm> // for std::atof 
#include <string>
#include <cmath> 

 int main( int argc , char **argv )
{
    mathematics::Triangle t{ 0 , 0 , 0 };
    
    if( argc == 1 )
    {
        // If argc == 1, then the user hasn't introduce
        // command line arguments.
        // we can give him a chance to input his parameters
        // using std::cin
        std::cin >> t.a >> t.b >> t.c;
    }
    else if( argc == 4 )
    {
        a = std::atof( argv[1] );
        b = std::atof( argv[2] );
      c= std::atof( argv[3] ) ;
    }
    else
    {
        std::cout << "Incorrect usage!" << std::endl;
        return 1; // Failure Code!
    }

    std::cout << mathematics::heron( t ) << std::endl;
    return 0; // Success Code!
}
