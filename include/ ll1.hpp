#ifndef MEMBER1_HPP
#define MEMBER1_HPP
namespace LL
{
    struct Node
{
    char data;
    Node* next;
};
    struct IntegersLL
{
    Node *head;
    Node *tail;
};
    struct CharsLL 
{
    Node *head;
    Node *tail;
};
    struct DoubleLL
{
    Node *head;
    Node *tail;
};
    void insertFront( CharsLL &item, char data )
{
   Node *newfront  = new Node;
   newfront->data = data;
   newfront->next = item.head;
   item.head = newfront;
}
    void insertBack( CharsLL &item , char newElement )
{
    Node *current = item.head;
    
    if( current == nullptr )
    {
        current = new Node{ newElement , nullptr };
        item.head = current;
    }
    else
    {
        while( current->next != nullptr )
        {
          current = current->next;
        }
        current->next = new Node{ newElement , nullptr };
    }
}
void insertFront( IntegersLL &list, int data )
{
   Node *newfront  = new Node;
   newfront->data = data;
   newfront->next = list.head;
   list.head = newfront;
}
void insertBack( IntegersLL &list , int newElement )
{
    Node *current = list.head;
    
    if( current == nullptr )
    {
        current = new Node{ newElement , nullptr };
        list.head = current;
    }
    else
    {
        while( current->next != nullptr )
        {
          current = current->next;
        }
        current->next = new Node{ newElement , nullptr };
    }
}
void insertFront( DoubleLL &item, double data )
{
   Node *newfront  = new Node;
   newfront->data = data;
   newfront->next = item.head;
   item.head = newfront;
}
    void insertBack( DoubleLL &item , double newElement )
{
    Node *current = item.head;
    
    if( current == nullptr )
    {
        current = new Node{ newElement , nullptr };
        item.head = current;
    }
    else
    {
        while( current->next != nullptr )
        {
          current = current->next;
        }
        current->next = new Node{ newElement , nullptr };
    }
}
}
namespace stack
{
    struct IntegersStack
    {
        int buffer[100];
        int top = -1;
    };
    void push( IntegersStack &stack , int data )
{
    ++stack.top;
    stack.buffer[ stack.top ] = data;
}
    struct CharsStack
    {
        char buffer[100];
        int top = -1; 
    };
    void push( CharsStack &stack , char data )
{
    ++stack.top;
    stack.buffer[ stack.top ] = data;
}
    struct DoubleStack
    {
        double buffer[100];
        int top = -1; 
    };
    void push( DoubleStack &stack , double data )
{
    ++stack.top;
    stack.buffer[ stack.top ] = data;
}
}
namespace queue
{
    struct IntegersQueue
    {
        int rear , front;
        int temp[100];
    };
    void enqueue( IntegersQueue &q , int newElement)
{
    q.temp[q.rear]=newElement;
    q.rear++;
}
    struct DoublesQueue
    {
        int rear , front;
        double temp[100];
    };
    void enqueue( DoublesQueue &q , int newElement)
{
    q.temp[q.rear]=newElement;
    q.rear++;
}
    struct CharsQueue
    {
        int rear , front;
        char temp[100];
    };
    void enqueue( CharsQueue &q , int newElement)
{
    q.temp[q.rear]=newElement;
    q.rear++;
}
}
#endif // MEMBER1_HPP
