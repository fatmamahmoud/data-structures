#ifndef MEMBER4_HPP
#define MEMBER4_HPP

namespace LL
{

    //IsEmpty Function

    //Integer

    struct IntNode 
    {
        int data;
        IntNode *next;
    };

    struct IntegerLL
    {
        IntNode *front ;
        IntNode *back = nullptr;
    };

    int back( IntegerLL &list )

    {
        IntNode *current = front ;
        if ( list.back == nullptr )
        {
            return list.back ;
        }
        else
        {
            current = current->next ;
        }
    }

    //Double 

     struct DoubleNode 
    {
        double data;
        DoubleNode *next;
    };

    struct DoubleLL
    {
        DoubleNode *front ;
        DoubleNode *back = nullptr;
    };

    double back( DoubleLL &list )

    {
        DoubleNode *current = front ;
        if ( list.back == nullptr )
        {
            return list.back ;
        }
        else
        {
            current = current->next ;
        }
    }

    //Char

    struct CharNode 
    {
        char data;
        CharNode *next;
    };

    struct CharLL
    {
        CharNode *front ;
        CharNode *back = nullptr;
    };

    char back( CharLL &list )

    {
        CharNode *current = front ;
        if ( list.back == nullptr )
        {
            return list.back ;
        }
        else
        {
            current = current->next ;
        }
    }

    //getAt Function 

    //Integer 

    int getAt( IntegerLL &list , int index )

    {
        IntNode *current = list.front;
        int counter = 0 ;

        while( current->next != nullptr )
        {
            current = current->next;
            counter = counter + 1;
            if( counter == index )
            {
                return current->data;
            }
        }
    }

    //Double

    double getAt( DoubleLL &list , double index )

    {
        DoubleNode *current = list.front;
        double counter = 0 ;

        while( current->next != nullptr )
        {
            current = current->next;
            counter = counter + 1;
            if( counter == index )
            {
                return current->data;
            }
        }
    } 

    //Char

    char getAt( CharLL &list , int index )

    {
        CharNode *current = list.front;
        int counter = 0 ;

        while( current->next != nullptr )
        {
            current = current->next;
            counter = counter + 1;
            if( counter == index )
            {
                return current->data;
            }
        }
    } 


}
 
namespace stack
{
    //***
    // stack using array 

    //integer

    struct IntegersStack
    {
        int data ;
        int size ;
        int top = -1;
    };

    bool isEmpty( IntegersStack &stack )
    {
        return ( stack.top == -1 );
    }

    //Char

    struct Charstack 
    {
        char data ;
        int size ;
        char top = -1;
    };

    bool isEmpty( Charstack &stack )
    {
        return ( stack.top == -1 );
    }

    //stack using linkedlist

    //integer

    struct IntNode 
    {
        int data;
        IntNode *next;
    };

    struct IntegerLL
    {
        IntNode *front = nullptr;
    };

    bool isEmpty( IntegerLL &stack )
    {
        return ( stack.front == nullptr );  
    }

    //Char

    struct CharNode 
    {
        char data;
        CharNode *next;
    };

    struct CharLL
    {
        CharNode *back = nullptr;
    };

    bool isEmpty( CharLL &stack )
    {
        return ( stack.back == nullptr );
    }


}

namespace queue
{
    // queue using linkedlist

    // Double 

    struct DoubleNode
    {
        double data;
        DoubleNode *next;
    };

    struct DoublesQueueLL
    {
        DoubleNode *back = nullptr; 
    };

    bool isEmpty( DoublesQueueLL &queue )
    {
        return ( queue.back == nullptr );
    }

    //Char

    struct CharNode
    {
        char data;
        CharNode *next;
    };

    struct CharQueueLL
    {
        CharNode *back = nullptr; 
    }; 

    bool isEmpty( CharQueueLL &queue )
    {
        return ( queue.back == nullptr );
    }


}

#endif // MEMBER4_HPP
