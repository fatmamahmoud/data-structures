#ifndef ARRAYS_HPP
#define ARRAYS_HPP

#include <iostream>
#include "mathematics.hpp"
namespace arrays{
void printAll(double *base,int arraySize)
{
    for (int i = 0; i <arraySize; ++i)
        std::cout << base[i] << std::endl;
}

struct DoubleArray 
{
   double *base;
   int arraySize;
};

void printAll( DoubleArray array)
{
    for (int i = 0; i < array.arraySize; ++i)
        std::cout << array.base[i] << std::endl;
}

double maxArray(double *base,int arraySize)
{
    for (int i = 0; i < arraySize; ++i)
    {
        double max = base[0];
        if (max < base[i])
        {
            max = base[i];
        }
        return max;
    }
}
 
double maxArray(DoubleArray array)
{
    for (int i = 0; i < array.arraySize; ++i)
    {
        double max = array.base[0];
        if (max < array.base[i])
        {
            max = array.base[i];
        }
        return max;
    }
}

double minArray(double *base, int arraySize)
{
    for (int i = 0; i < arraySize; ++i)
    {
        double min = base[0];
        if (min > base[i])
        {
            min = base[i];
        }
        return min;
    }
} 

double minArray(DoubleArray array )
{
    for (int i = 0; i < array.arraySize; ++i)
    {
        double min =array. base[0];
        if (min > array.base[i])
        {
            min = array.base[i];
        }
        return min;
    }
}

double meanArray(double *base, int arraySize)
{
    double sum = 0;
    for (int i = 0; i < arraySize; ++i)
    {
        sum = sum + base[i];
    }
    return sum / arraySize;
} 


double meanArray( DoubleArray array)
{
    double sum = 0;
    for (int i = 0; i <array. arraySize; ++i)
    {
        sum = sum +array. base[i];
    }
    return sum / array.arraySize;
}
 
double varianceArray(double *base, int arraySize)
{
    double var=0;
    for (int i = 0; i < arraySize; ++i)
    {
        double mean =arrays::meanArray(&base, arraySize);
        double q=q+((mean-base[i])*(mean-base[i]));
        var=(1/arraySize)*q;
        
    }
    return var;
}

 
double varianceArray(DoubleArray array )
{
    double var=0;
    for (int i = 0; i < array.arraySize; ++i)
    {
        double mean =arrays::meanArray(&array.base, array.arraySize);
        double q=q+((mean-array.base[i])*(mean-array.base[i]));
        var=(1/array.arraySize)*q;
        
    }
    return var;
}
}



#endif // ARRAYS_HPP
