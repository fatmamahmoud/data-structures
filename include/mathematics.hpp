#ifndef MATHEMATICS_HPP
#define MATHEMATICS_HPP
#include <string>
#include <cmath> // for std::sqrt

namespace mathematics
{
    double calculation(double a, double operation, double b)
    {
        if (operation == '+')
        {
            return a + b;
        }
        else if (operation == '-')
        {
            return a - b;
        }
        else if (operation == '*')
        {
            return a * b;
        }
        else
        {
            return a / b;
        }
    }
    struct Triangle
    {
        double a;
        double b;
        double c;
    };
    double heron(Triangle t)
    {
        double s = 0;
        double A = 0;
        s = (t.a + t.b + t.c) / 2;
        A = sqrt(s * (s - t.a) * (s - t.b) * (s - t.c));

        return A;
    }

    double heron(double a, double b, double c)
    {
        double s = 0;
        double A = 0;
        s = (a + b + c) / 2;
        A = sqrt(s * (s - a) * (s - b) * (s - c));

        return A;
    }
}

#endif
